// Fetch
// "fetch()" method starts the process of fetching a resource from a server
// "fetch()" method returns a promise that resolves to a Response object
fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json()" method from the "response object" to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request
//.then((json) => console.log(json))
// Print the converted JSON value from the "fetch" request
.then((data) => showPosts(data));


// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    // Prevents the page from reloading
    // prevents default behavior of event
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
        // Sets the method of the "Request" object to "POST" following REST API
        // Default method is GET
        method: 'POST',
        // Sets the header data of the "Request" object to be sent to the backend
        // Specified that the content will be in a JSON structure
        headers: {'Content-type': 'application/json; charset=UTF-8'},
        // Sets the content/body data of the "Request" object to be sent to the backend
        // JSON.stringify converts the object data into a stringified JSON
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully added!');
        // Resets the input fields
        document.querySelector('#txt-title').value = null;
        document.querySelector('#txt-body').value = null;
    });
});


// RETRIEVE POSTS
const showPosts = (posts) => {
    // "postEntries" - variable that will contain all the posts
    let postEntries = "";

    posts.forEach((post) => {
        // "+=" - adds tha value of the right operand to a variable and assigns the result to the variable
        // "div" - division on where to place the value of title and posts
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });
    // To check what is stored in the properties of variables
    //console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// EDIT POST
const editPost = (id) => {
    // The function first uses the querySelector() method to get the element with the id "#post-title-${id}" and "#post-body-${id}" and assigns its innerHTML property to the title variable with the same body
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
    // To remove the "disabled" attribute from the update button
    // removeAttributes() - removes the attribute with the specified name from the element
    document.querySelector('#btn-submit-update').removeAttribute('disabled');
};

// UPDATE POST
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        // Sets the header data of the "request" object to be sent to the backend
        headers: {'Content-type': 'application/json; charset=UTF-8'}
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully updated!')

    // Reset the edit post form input fields
    document.querySelector('#txt-edit-id').value = null;
    document.querySelector('#txt-edit-title').value = null;
    document.querySelector('#txt-edit-body').value = null;

    // .setAttribute - sets an attribute to an HTML element
    document.querySelector('#btn-submit-update').setAttribute('disabled', true);
    })
});


// DELETE post
const deletePost = (id) => {
   fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: 'DELETE'})

   document.querySelector(`#post-${id}`).remove()
}